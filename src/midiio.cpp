//
// Created by dhuck on 1/15/21.
//

#include "midiio.h"
MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI0)
MIDI_CREATE_INSTANCE(HardwareSerial, Serial2, MIDI1)
MIDI_CREATE_INSTANCE(HardwareSerial, Serial3, MIDI2)
MIDI_CREATE_INSTANCE(HardwareSerial, Serial4, MIDI3)


MidiIo::MidiIo(Preset *p) : preset(p) {
    io[0] = &MIDI0;
    io[1] = &MIDI1;
    io[2] = &MIDI2;
    io[3] = &MIDI3;

    for (auto curr_midi : io) {
        curr_midi->begin(MIDI_CHANNEL_OMNI);
        curr_midi->turnThruOff();
    }
}



void MidiIo::flush() {
    for (uint8_t i = 0; i < NUM_INPUTS; i++) {
        parseMidi(i);
    }
}

void MidiIo::parseMidi(uint8_t in) {
    auto curr_midi = io[in];
    if (curr_midi->read()) {
        auto tp = curr_midi->getType();
        auto ch = curr_midi->getChannel();
        auto d1 = curr_midi->getData1();
        auto d2 = curr_midi->getData2();
        Serial.printf("Got one!\n -- Input: %d, Channel: %d, Type: %d\n", in, ch, (byte) tp);
        for (int i = 0; i < NUM_OUTPUTS; i++) {
            auto out = preset->getSettings(i);
            if (out->input[in]) {
                auto currMidi = io[i];
                if (applyFilters(tp, ch, in)) {
                    currMidi->send(tp, d1, d2, ch);
                    Serial.printf("    -- Sent message to %d\n", i);
                }
            }
        }
    }
}

bool MidiIo::applyFilters(byte type, uint8_t ch, uint8_t in) {
    auto out = preset->getSettings(in);
    if (type == midi::NoteOn) {
        Serial.println("    -- Note On Message!");
        return out->channels[ch].note;
    }
    if (type == midi::ControlChange) {
        Serial.println("    -- CC Message!");
        return out->channels[ch].cc;
    }
//    if (type == midi::Clock) {
//        Serial.println("    -- Clock Message!");
//        return out->;
//    }
    return true;
}
