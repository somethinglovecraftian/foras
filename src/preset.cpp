//
// Created by dhuck on 1/14/21.
//

#include "preset.h"

Preset::Preset() {
    for (auto & setting : settings) {
        for (auto & channel : setting.channels) {

            channel.cc = true;
            channel.note = true;
        }
        for (bool & in : setting.input) {
            in = false;
        }
        clockSource = EXTERNAL_0;
        tempo = 120;
    }
    name[0] = 'M';
    name[1] = 'I';
    name[2] = 'D';
    name[3] = 'I';
    name[4] = ' ';
    name[5] = 'F';
    name[6] = 'L';
    name[7] = 'T';
    clockSource = EXTERNAL_0;
}

void Preset::pack() {
    uint8_t pos = 0;
    uint8_t i, j;
    byte temp;

    // pack the name
    for (auto & letter : name) {
        packed_preset[pos++] = letter;  //pos = 8
    }

    // pack the clockSource and tempo
    packed_preset[pos++] = clockSource;
    packed_preset[pos++] = tempo;      // pos = 10

    // time to do the outputs!
    for (i = 0; i < NUM_OUTPUTS; i++) {
        Output *out = &settings[i];
        // encode the inputs. One byte per input, enabled can be calculated from this
        // during unpacking
        temp = 0;
        for (j = NUM_INPUTS; j > 0; j--) {
            temp |= (out->input[j - 1] & 0x01) << (j - 1);
        }
        packed_preset[pos++] = temp; // pos += 1

        // encode channels
        Channel *channels = out->channels;
        for (j = 0; j < NUM_CHANNELS; j += 2) {  // pos += 8
            temp = 0;
            temp = (channels[j].cc)    ? temp | 0x10 : temp;
            temp = (channels[j].note)  ? temp | 0x20 : temp;
            temp = (channels[j].sysex) ? temp | 0x40 : temp;
            temp = (channels[j + 1].cc)    ? temp | 0x01 : temp;
            temp = (channels[j + 1].note)  ? temp | 0x02 : temp;
            temp = (channels[j + 1].note)  ? temp | 0x04 : temp;
            packed_preset[pos++] = temp;
        }
    }
    for (; pos < PRESET_BUFFER; pos++) {
        packed_preset[pos] = 0;
    }
}

void Preset::unpack() {
    uint8_t pos = 0;
    int i, j;
    for (auto & letter : name) {
        letter = (char) packed_preset[pos++];
    }
    clockSource = static_cast<ClockSource>(packed_preset[pos++]);
    tempo = packed_preset[pos++];

    for (i = 0; i < NUM_OUTPUTS; i++) {
        Output *out = &settings[i];

        for (j = 0; j < NUM_INPUTS; j++) {
            out->input[j] = (packed_preset[pos] << j) & 0x01;
        }
        pos++;
        Channel *channels = out->channels;
        for (j = 0; j < NUM_CHANNELS; j += 2) {
            channels[j].cc          = packed_preset[pos] & 0x10;
            channels[j].note        = packed_preset[pos] & 0x20;
            channels[j].sysex       = packed_preset[pos] & 0x40;

            channels[j + 1].cc      = packed_preset[pos] & 0x01;
            channels[j + 1].note    = packed_preset[pos] & 0x02;
            channels[j + 1].sysex   = packed_preset[pos] & 0x04;
            pos++;
        }
    }
}

Output *Preset::getSettings(uint8_t row) {
    return &settings[row];
}

void Preset::updateSetting(Settings setting, uint8_t pos, uint8_t val) {

}

void Preset::load() {
    pack();
    uint16_t base = PRESET_START_ADDRESS;
    for(uint16_t i = 0; i < PRESET_BUFFER; i++) {
        packed_preset[i] = EEPROM.read(base + i);
    }
}

void Preset::store() {

}

void Preset::toggleSetting(Settings setting, uint8_t pos) {
//    if (settings[setting][pos] > 0) {
//        settings[setting][pos] = 0;
//    } else {
//        settings[setting][pos] = 1;
//    }
}
