// Set up Dev environment
#define DEV false
#define SERIAL_RATE 115200

// Other definitions
#define LED_PIN 13
#define PERIOD_2_KHZ 63
#define PERIOD_1_KHZ 125
#define PERIOD_30_HZ 33333
#define PERIOD_15_HZ 66666

#include "screen.h"
#include "interface.h"
#include "routing.h"
#include "midiio.h"
#include "global_settings.h"

#include <IntervalTimer.h>
#include <Arduino.h>

Screen *screen = new Screen();


void displayHandler() {
    screen->writeDisplay();
}

void bootNotification() {
    int init_delay = 200;
    int flashes = 4;

    for (int i = 0; i < flashes; i++) {
        init_delay = init_delay / 2;
        digitalWrite(LED_PIN, HIGH);
        delay(init_delay);
        digitalWrite(LED_PIN, LOW);
        delay(init_delay);
    }
//    if (DEV) digitalWrite(LED_PIN, HIGH);
}

void setup(IntervalTimer *displayTimer) {
    pinMode(OUTPUT, LED_PIN);
#if DEV
        while (!Serial); // Wait for serial monitor to connect
        Serial.begin(SERIAL_RATE);
        Serial.printf("YOU ARE IN DEVELOPMENT MODE. SHIT WON'T WORK RIGHT WITH THIS ON, BUT IT'LL DO...\n");
        for (int i = 0; i < 4; i++) {
            Serial.println();
        }
        Serial.println("Beginning displayHandler");
#endif
    displayTimer->begin(displayHandler, PERIOD_15_HZ);
    displayTimer->priority(255);

    bootNotification();
}

int main() {
    auto displayTimer = new IntervalTimer();
    setup(displayTimer);

#if DEV
    Serial.println("Setting up commandQueue, displayTimer, preset, midiIO, interface, Routing Page...");
#endif
    auto commandQueue = new ControlQueue();
    auto preset = new Preset();
    auto midiIO = new MidiIo(preset);
    auto interface = new Interface(commandQueue);
    auto routingPage = new Routing(commandQueue, screen, preset);
    routingPage->activate();

    // Main Loop

#if DEV
    Serial.println("Beginning Main Loop...");
#endif

    for (;;) {
        interface->update();
        routingPage->checkMessages();
        midiIO->flush();
    }
}