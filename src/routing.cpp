//
// Created by dhuck on 1/13/21.
//

#include "routing.h"

Routing::Routing(ControlQueue *q, Screen *s, Preset *p)
        : commandQueue(q),
          screen(s),
          active(false),
          preset(p) {

}

void Routing::setRows() {
    for (uint8_t row = 0; row < ROWS; row++) {
        rows[row][0] = '1' + row;
        auto outSettings = preset->getSettings(row);
        bool filters[NUM_FILTERS] = {outSettings->channels[0].cc,
                                     outSettings->channels[0].note,
                                     true};

        setFilterInfo(row, filters);
        setOutputInfo(row, outSettings->input);

        for (int j = 0; j < ROWS; j++) {
            rows[j][ROUTING_ACTIVE_OUT + j] = '1' + j;
        }
    }
}

void Routing::setFilterInfo(uint8_t row, const bool* filters) {
    uint8_t pos = ROUTING_FILTER;
    for (int i = 0; i < NUM_FILTERS; i++) {
        for (int j = 0; j < FILTER_WIDTH; j++) {
            if (filters[i] == 1) {
                rows[row][pos] = filterText[i][j];
            } else {
                rows[row][pos] = ' ';
            }
            pos++;
        }
    }
}

void Routing::setOutputInfo(uint8_t row, const bool* outputs) {
    uint8_t pos = ROUTING_ACTIVE_OUT;
    for (int out = 0; out < NUM_OUTPUTS; out++) {
        if (outputs[out]) {
            rows[row][pos] = '1' + out;
        } else {
            rows[row][pos] = ' ';
        }
        pos++;
    }
}

void Routing::activate() {
    active = true;
    screen->setHeader("Router;");
    setRows();
    for (int i = 0; i < ROWS; i++) {
        screen->setRow(i, rows[i]);
    }
    screen->setFooterButton(0, "#@?!;");
    screen->setFooterButton(1, "FILT;");
    screen->setFooterButton(2, "OUTS;");
    screen->setTotalRows(ROWS);
}

void Routing::deactivate() {
    active = false;
}

void Routing::checkMessages() {
    while (!commandQueue->empty()) {
        Controls command = commandQueue->front();
        commandQueue->pop();
        switch (command) {
            case Controls::ENC_0_UP:
                screen->scrollUp();
                break;
            case Controls::ENC_0_DOWN:
                screen->scrollDown();
                break;
            case Controls::ENC_1_UP:
                filterSelection(true);
                break;
            case Controls::ENC_1_DOWN:
                filterSelection(false);
                break;
            case Controls::ENC_2_UP:
                outputSelection(true);
                break;
            case Controls::ENC_2_DOWN:
                outputSelection(false);
                break;
            case Controls::SWITCH_6:
                toggleSelection(true);
                break;
            case Controls::SWITCH_7:
                toggleSelection(false);
                break;
            default:
                break;
        }
        activeRow = screen->getActiveRow();
    }
}

void Routing::filterSelection(bool up) {
    if (up && activeFilter + 1 < NUM_FILTERS) {
        activeFilter++;
    } else if (!up && activeFilter != 0) {
        activeFilter--;
    }

    screen->displayRow(activeRow);
    screen->drawBox(activeFilter * FILTER_WIDTH + ROUTING_FILTER, activeRow, FILTER_WIDTH);
}

void Routing::outputSelection(bool up) {
    if (up && activeOutput + 1 < NUM_OUTPUTS) {
        activeOutput++;
    } else if (!up && activeOutput != 0) {
        activeOutput--;
    }

    screen->displayRow(activeRow);
    screen->drawBox(activeOutput + ROUTING_ACTIVE_OUT, activeRow, 1);
}

void Routing::toggleSelection(bool filter) {
    if (filter) {
        preset->toggleSetting(FILTERS[activeRow], activeFilter);
//        setFilterInfo(activeRow);
    } else {
        preset->toggleSetting(OUTPUTS[activeRow], activeOutput);
//        setOutputInfo(activeRow);
    }
    screen->setRow(activeRow, rows[activeRow]);
}
