#include <climits>
//
// Created by dhuck on 1/11/21.
//

#include "screen.h"

Screen::Screen() : oled(OLED_SCREEN_WIDTH, OLED_SCREEN_HEIGHT, &Wire, -1) {
    oled.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.clearDisplay();
    activeRow = 0;
    totalRows = 5; //TODO: This needs to be moved to activate();
    displayHeader();
    displayFooter();
}

void Screen::displayRows() {
    for (int r = startRow; r < startRow + SCREEN_ROWS; r++) {
        displayRow(r);
    }
}

void Screen::displayRow(int row) {
    row = ROW_START + (ROW_HEIGHT * row);
    clearLine(0, row);
    oled.setCursor(CURSOR_X, row);
    for (char &ch : rows[row]) {
        oled.print(ch);
    }
    if (row == activeRow) {
        oled.fillRect(0, row, OLED_SCREEN_WIDTH, ROW_HEIGHT, INVERSE);
    }
}

void Screen::writeDisplay() {
    oled.display();
}

void Screen::clearLine(int x, int y) {
    oled.fillRect(x, y, OLED_SCREEN_WIDTH - x, ROW_HEIGHT, BLACK);
}

void Screen::displayHeader() {
    clearLine(0, 0);
    oled.setCursor(CURSOR_X, 0);
    for (char &ch : header) {
        oled.print(ch);
    }
    oled.drawFastHLine(0, ROW_HEIGHT, OLED_SCREEN_WIDTH - 1, WHITE);
}

void Screen::setHeader(const char *head) {
    int i = 0;
    while (i < ROW_WIDTH && head[i] != ESCAPE_CHAR) {
        header[i] = head[i];
        i++;
    }
    for (; i < ROW_WIDTH; i++) {
        header[i] = ' ';
    }
    displayHeader();
}

void Screen::setRow(int row, const char text[]) {
    int i = 0;
    while (i < ROW_WIDTH && text[i] != ESCAPE_CHAR) {
        rows[row][i] = text[i];
        i++;
    }
    for (; i < ROW_WIDTH; i++) {
        rows[row][i] = ' ';
    }
    displayRow(row);
}

void Screen::clearData() {
    oled.clearDisplay();
    displayHeader();
}

void Screen::setFooterButton(int btn, const char text[]) {
    int i = 0;
    while (i < BUTTON_WIDTH && text[i] != ESCAPE_CHAR) {
        footerButtons[btn][i] = text[i];
        i++;
    }
    for (; i < BUTTON_WIDTH; i++) {
        footerButtons[btn][i] = ' ';
    }
    displayFooter();
}

void Screen::displayFooter() {
    oled.drawFastHLine(0, FOOTER_START - SPACING, OLED_SCREEN_WIDTH, WHITE);
    clearLine(CURSOR_X, FOOTER_START);
    auto spacing = OLED_SCREEN_WIDTH / BUTTONS;
    for (int btns = 1; btns < BUTTONS; btns++) {
        auto x = btns * spacing;
        oled.drawFastVLine(x, FOOTER_START - SPACING, OLED_SCREEN_HEIGHT - FOOTER_START + 2, WHITE);
    }
    for (int i = 0; i < BUTTONS; i++) {
        auto x = i * spacing + (spacing / 2) - (spacing / 4);
        oled.setCursor(x, FOOTER_START);
        for (char &ch : footerButtons[i]) {
            oled.print(ch);
        }
    }
}

boolean Screen::scrollScreen(boolean up) {
    if ((activeRow == totalRows - 1 && up) || (activeRow == 0 && !up)) {
        return false;
    }
    if (up)
        activeRow++;
    else
        activeRow--;
    if (activeRow > (startRow + SCREEN_ROWS)) {
        startRow++;
    } else if (activeRow < startRow) {
        startRow--;
    }
    Serial.printf("Active Row: %d", activeRow);
    displayRows();
    return true;
}

boolean Screen::scrollUp() {
    return scrollScreen(true);
}

boolean Screen::scrollDown() {
    return scrollScreen(false);

}

void Screen::pressFooterButton(int but) {
    if (but < BUTTONS) {
        int width = OLED_SCREEN_WIDTH / BUTTONS;
        int x = but * width;
        oled.fillRect(x, FOOTER_START - 1, width, ROW_HEIGHT + 2, INVERSE);
    }
}

void Screen::setTotalRows(int n) {
    totalRows = n;
}

uint8_t Screen::getActiveRow() {
    return activeRow;
}

void Screen::drawBox(uint8_t col, uint8_t row, uint8_t cols) {
    auto x = col * SCREEN_CHAR_WIDTH;
    auto y = row * ROW_HEIGHT + ROW_START;
    auto w = cols * SCREEN_CHAR_WIDTH;
    oled.fillRect(x, y, w, ROW_HEIGHT, INVERSE);
}

