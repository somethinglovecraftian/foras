//
// Created by dhuck on 1/13/21.
//

#include "interface.h"


Interface::Interface(ControlQueue *q) : commandQueue(q) {
    for (int i = 0; i < ENCODERS; i++) {
        encoders[i] = new Encoder(AVAILABLE[i][0], AVAILABLE[i][1]);
        encoderMillis[i] = 0;
    }
    switches = new FortyFiftyOne(SWITCHES_SIGNAL, INPUT_PULLDOWN, DIGITAL, SWITCHES_MUX_A, SWITCHES,
                                               UNDEFINED);
    for (int i = 0; i < SWITCHES; i++) {
        switchMillis[i] = 0;
    }
}



void Interface::update() {
    updateEncoders();
    updateSwitches();
}

void Interface::updateEncoders() {
    for (uint8_t curr = 0; curr < ENCODERS; curr++) {
        int32_t val = encoders[curr]->read() / ENCODER_AMOUNT; // each detente increases or decreases the encoder by a set amount
        if (val > en_val[curr]) {
            commandQueue->push(controlEncodersUp[curr]);
        } else if (val < en_val[curr]) {
            commandQueue->push(controlEncodersDown[curr]);
        }
        en_val[curr] = val;
    }

}

void Interface::updateSwitches() {
    for (int s = 0; s < SWITCHES; s++) {
        auto currTime = millis();
        if (switches->read(s) != 0) {
            if (currTime - switchMillis[s] > INTERFACE_TIMEOUT) {
                commandQueue->push(controlSwitches[s]);
                switchMillis[s] = currTime;
            }
        }
    }
}
