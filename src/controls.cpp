//
// Created by dhuck on 5/20/21.
//

#include "controls.h"

void ControlQueue::push(Controls c) {
    auto *command = new Command;
    command->control = c;
    command->next = nullptr;
    if (size == 0) {
        this->head = command;
        this->tail = command;
    } else {
        tail->next = command;
        tail = command;
    }
    size++;
}

Controls ControlQueue::pop() {
    if (size == 0) {
        return NONE;
    }
    auto *tmp = head;
    this->head = head->next;
    Controls c = tmp->control;
    delete tmp;
    size--;
    return c;
}

bool ControlQueue::empty() {
    return size == 0;
}

Controls ControlQueue::front() {
    return head->control;
}