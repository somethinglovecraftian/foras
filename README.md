# FORAS

Foras is a Midi project in development by LoveCraftWork. It is a a MIDI splitter/merger with filters, presets,
and on-the-fly editing of these presets. It aims to be a low-cost DIY or assembled device that will give electronic
musicians quick access to their MIDI configurations, whether in the studio, in a jam session with others, or live on 
stage.

More information can be found at the [project notion page](https://www.notion.so/Foras-36c4d45861db42b5a2319f5692ba2cbb).

## Specification

* Each input will have the ability to send data to each output. Should multiple inputs send to the same output, the
output will merge the data from the multiple inputs.
  
* Each input will have a set of filters associated with the input. These filters give the performer the ability to 
filter out certain channels, cc controls, or note on/off messages for each input.
  
* The control surface will consist of a screen, three push encoders and a button to navigate back a page. Each 
encoder will correspond to a column on the screen.
  
* The device will be able to hold _at least_ 128 presets. These can be accessed by pressing back on the main screen or by 
using a bank/preset combination on MIDI in 1, channel 10 by default. This should be user addressable in the global 
settings.
  