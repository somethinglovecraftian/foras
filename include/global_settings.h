//
// Created by dhuck on 1/13/21.
//
#include <memory>
#include "screen.h"

#ifndef FORAS_GLOBAL_SETTINGS_H
#define FORAS_GLOBAL_SETTINGS_H

class GlobalSettings {
private:

public:
    GlobalSettings(std::shared_ptr<Screen>) {};
};

#endif //FORAS_GLOBAL_SETTINGS_H
