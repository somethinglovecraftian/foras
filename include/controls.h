/*
 * Created by dhuck on 1/13/21.
 */

#include "Arduino.h"

#ifndef FORAS_CONTROLS_H
#define FORAS_CONTROLS_H

/**
 * Enumeration for the different controls on the faceplate. Current version
 * has max for proto box, will be paired down in final release
 *
 * TODO: finalize the number of controls available.
 */

enum Controls {
    SWITCH_0,
    SWITCH_1,
    SWITCH_2,
    SWITCH_3,
    SWITCH_4,
    SWITCH_5,
    SWITCH_6,
    SWITCH_7,
    ENC_0_UP,
    ENC_1_UP,
    ENC_2_UP,
    ENC_3_UP,
    ENC_0_DOWN,
    ENC_1_DOWN,
    ENC_2_DOWN,
    ENC_3_DOWN,
    NONE = 0xFF
};
/**
 * Simple command struct holding the control last seen and a pointer to the
 * next control in the Queue.
 */
struct Command {
    Controls control;
    Command *next;
};

/**
 * Standard Queue class to queue up control messages. Implemented as a FIFO queue.
 */
class ControlQueue {
private:
    Command *head;
    Command *tail;
    uint32_t size;

public:
    ControlQueue() : head(nullptr), tail(nullptr), size(0) {};

    /**
     * push() - pushes a command onto the end of the command queue.
     */
    void push(Controls);

    /**
     * pops a control message off of the front of the command queue
     * @return Controls type control message to be me handled next.
     */
    Controls pop();

    /**
     * Checks to see if the queue is empty or not.
     * @return true if empty, false otherwise
     */
    bool empty();

    /**
     * Look at the control at the head of the queue, but do not pop.
     * @return Controls type control message at head of queue.
     */
    Controls front();
};



#endif //FORAS_CONTROLS_H
