/**
 * Created by dhuck on 1/11/21.
 */

#ifndef FORAS_INTERFACE_H
#define FORAS_INTERFACE_H

#define MAX_ENCODERS 4
#define MAX_SWITCHES 8
#define ENCODERS 3
#define SWITCHES 8

#define SWITCHES_MUX_A 20
#define SWITCHES_SIGNAL 6

// Each detente in the encoder increases or decreases the value by 4.
#define ENCODER_AMOUNT 4

// Lock on subsequent switch presses
#define INTERFACE_TIMEOUT 150

#include "Encoder.h"
#include <Arduino.h>
#include "controls.h"
#include "FortyFiftyOne.h"

/**
 * Interface class handles all of the interfacing needs of the unit. It organizes
 * communication between the encoders and switches, the current patch settings,
 * and the midi IO.
 *
 * This current implementation relies on multiplexing to work, but this may change
 * in future releases as the number of hardware controls is finalized.
 */
class Interface {
private:
    const uint8_t AVAILABLE[4][2] = {{2, 3}, {4, 5}, {9, 10}, {8, 9}};
    Controls controlEncodersUp[MAX_ENCODERS] = {ENC_0_UP, ENC_1_UP, ENC_2_UP, ENC_3_UP};
    Controls controlEncodersDown[MAX_ENCODERS] = {ENC_0_DOWN, ENC_1_DOWN, ENC_2_DOWN, ENC_3_DOWN};
    Controls controlSwitches[SWITCHES] = {SWITCH_0, SWITCH_1, SWITCH_2, SWITCH_3, SWITCH_4, SWITCH_5, SWITCH_6, SWITCH_7};
    uint32_t en_val[ENCODERS]{};

    // holds pointers to each Encoder object
    Encoder *encoders[ENCODERS];

    // pointer to switches which reside behind the 4051-library
    FortyFiftyOne *switches;
    int32_t encoderMillis[ENCODERS];
    // Used for debouncing
    int32_t switchMillis[SWITCHES];

    ControlQueue *commandQueue;

    /**
     * updateEncoders() - Updates the stored value of the encoders and pushes the
     * appropriate command onto the commandQueue. Checks the stored value against
     * the reading of an encoder object to see if the encoder has been moved CW
     * (up) or CCW (down).
     */
    void updateEncoders();

    /**
     * updateSwitches() - checks the time that has passed since the last pressing
     * of each switch and enqueues a command message if time is above the
     * INTERFACE_TIMEOUT threshold.
     */
    void updateSwitches();

public:
    explicit Interface(ControlQueue *);
    /**
     * wrapper for private functions. Calls updateEncoders() and updateSwitches()
     */
    void update();
};

#endif //FORAS_INTERFACE_H
