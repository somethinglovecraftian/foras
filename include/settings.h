//
// Created by dhuck on 1/13/21.
//

#ifndef FORAS_SETTINGS_H
#define FORAS_SETTINGS_H


enum Settings {
    MIDI_IN_0_OUTPUTS,  // 0x00
    MIDI_IN_1_OUTPUTS,  // 0x01
    MIDI_IN_2_OUTPUTS,  // 0x02
    MIDI_IN_3_OUTPUTS,  // 0x03
    MIDI_IN_4_OUTPUTS,  // 0x04
    MIDI_IN_5_OUTPUTS,  // 0x05
    MIDI_IN_6_OUTPUTS,  // 0x06
    MIDI_IN_7_OUTPUTS,  // 0x07
    MIDI_IN_8_OUTPUTS,  // 0x08
    MIDI_CLOCK_OUT,     // 0x09
    CH_01A_FILTERS,     // 0x0A
    CH_01B_FILTERS,     // 0x0B
    CH_02A_FILTERS,     // 0x0C
    CH_02B_FILTERS,     // 0x0D
    CH_03A_FILTERS,     // 0x0E
    CH_03B_FILTERS,     // 0x0F
    CH_04A_FILTERS,     // 0x10
    CH_04B_FILTERS,     // 0x11
    CH_05A_FILTERS,     // 0x12
    CH_05B_FILTERS,     // 0x13
    CH_06A_FILTERS,     // 0x14
    CH_06B_FILTERS,     // 0x15
    CH_07A_FILTERS,     // 0x16
    CH_07B_FILTERS,     // 0x17
    CH_08A_FILTERS,     // 0x18
    CH_08B_FILTERS,     // 0x19
    CH_09A_FILTERS,     // 0x1A
    CH_09B_FILTERS,     // 0x1B
    CH_10A_FILTERS,     // 0x1C
    CH_10B_FILTERS,     // 0x1D
    CH_11A_FILTERS,     // 0x1E
    CH_11B_FILTERS,     // 0x1F
    CH_12A_FILTERS,     // 0x20
    CH_12B_FILTERS,     // 0x21
    CH_13A_FILTERS,     // 0x22
    CH_13B_FILTERS,     // 0x23
    CH_14A_FILTERS,     // 0x24
    CH_14B_FILTERS,     // 0x25
    CH_15A_FILTERS,     // 0x26
    CH_15B_FILTERS,     // 0x27
    CH_16A_FILTERS,     // 0x28
    CH_16B_FILTERS,     // 0x29
    MIDI_IN_0_FILTERS,  // 0x3A
    MIDI_IN_1_FILTERS,  // 0x3B
    MIDI_IN_2_FILTERS,  // 0x3C
    MIDI_IN_3_FILTERS,  // 0x3D
    MIDI_IN_4_FILTERS,  // 0x3E
    MIDI_IN_5_FILTERS,  // 0x3F
    MIDI_IN_6_FILTERS,  // 0x40
    MIDI_IN_7_FILTERS,  // 0x41
    MIDI_IN_8_FILTERS,  // 0x42
    PRESET_CHAR_0,      // 0x43
    PRESET_CHAR_1,      // 0x44
    PRESET_CHAR_2,      // 0x45
    PRESET_CHAR_3,      // 0x46
    PRESET_CHAR_4,      // 0x47
    PRESET_CHAR_5,      // 0x48
    PRESET_CHAR_6,      // 0x49
    PRESET_CHAR_7,      // 0x4A
    PRESET_CHAR_8,      // 0x4B
    PRESET_CHAR_9,      // 0x4C
    PRESET_CHAR_A,      // 0x4D
    PRESET_CHAR_B,      // 0x4E
    PRESET_CHAR_C,      // 0x4F
    PRESET_CHAR_D,      // 0x50
    PRESET_CHAR_E,      // 0x51
    PRESET_CHAR_F       // 0x52
};


const Settings OUTPUTS[] = {
        MIDI_IN_0_OUTPUTS,
        MIDI_IN_1_OUTPUTS,
        MIDI_IN_2_OUTPUTS,
        MIDI_IN_3_OUTPUTS,
        MIDI_IN_4_OUTPUTS,
        MIDI_IN_5_OUTPUTS,
        MIDI_IN_6_OUTPUTS,
        MIDI_IN_7_OUTPUTS,
        MIDI_IN_8_OUTPUTS
};

const Settings FILTERS[] = {
        MIDI_IN_0_FILTERS,
        MIDI_IN_1_FILTERS,
        MIDI_IN_2_FILTERS,
        MIDI_IN_3_FILTERS,
        MIDI_IN_4_FILTERS,
        MIDI_IN_5_FILTERS,
        MIDI_IN_6_FILTERS,
        MIDI_IN_7_FILTERS,
        MIDI_IN_8_FILTERS,
};

#endif //FORAS_SETTINGS_H
