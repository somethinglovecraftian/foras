//
// Created by dhuck on 1/11/21.
//

#ifndef FORAS_SCENE_H
#define FORAS_SCENE_H

#include "Arduino.h"

// sets the y-coordinate for each row of text
#define HEADER 0
#define ROW_1 9
#define ROW_2 18
#define ROW_3 27
#define ROW_4 36
#define ROW_5 45
#define FOOTER 54
#define MAX_ROWS 5

// CURRENTLY FOR TESTING (???)
// TODO: figure out what this was supposed to be on midi2dmx
enum Row {
    ONE   = 9,
    TWO   = 18,
    THREE = 27,
    FOUR  = 36,
    FIVE  = 45
};

class Scene {
private:
    char *header;
    char *rows[MAX_ROWS];
    char *footer[];

public:
    Scene();
    void setRow(int i);
    void setHeader(char head[]);
    void setFooter(char foot[]);
    void drawScene();
};


#endif //FORAS_SCENE_H
