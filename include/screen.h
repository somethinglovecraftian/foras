//
// Created by dhuck on 1/11/21.
//

#ifndef FORAS_SCREEN_H
#define FORAS_SCREEN_H

#include "Arduino.h"
#include "Wire.h"
#include "Adafruit_SSD1306.h"
#include "Adafruit_GFX.h"
#include <string>
#include <array>

#define OLED_SCREEN_HEIGHT  64
#define OLED_SCREEN_WIDTH   128
#define OLED_ADDR 0x3C

#define CURSOR_X 1
#define ROW_WIDTH 21
#define ROW_START 12
#define ROW_HEIGHT 8
#define SCREEN_BUFFER 127
#define SCREEN_ROWS 5
#define FOOTER_START 56
#define SPACING 2
#define BUTTONS 3
#define BUTTON_WIDTH 5
#define SCREEN_CHAR_WIDTH 6

/**
 * Screen is a wrapper for the Adafruit graphics and screen libraries. This
 * handles the various shapes and text for the screen. Made to be portable
 * across several LOVECRAFTWORK products.
 */
class Screen {
private:
    const char ESCAPE_CHAR = ';';
    Adafruit_SSD1306 oled;
    int startRow = 0;
    int activeRow = 0;
    int totalRows;
    char header[ROW_WIDTH]{};
    char footerButtons[BUTTONS][BUTTON_WIDTH]{};
    char rows[SCREEN_BUFFER][ROW_WIDTH]{};

    void setupScreen();



    boolean scrollScreen(boolean);

public:
    /**
     * Simple constructor. Initiates oled, and sets the default settings.
     * Default destructor.
     */
    Screen();
    ~Screen() = default;

    /**
     * getActiveRow() - getter for the active Row
     * @return unsigned char
     */
    uint8_t getActiveRow();

    /**
     * clearLine() - clears a line starting at X at height y.
     * @param x Horizontal location to begin the clearing
     * @param y Row to be erased.
     */
    void clearLine(int x, int y);

    /**
     * displayHeader() - displays the header information at the top of the
     * screen.
     */
    void displayHeader();

    /**
     * displayRows() - draws the individual rows on the screen in text-based
     * menus.
     */
    void displayRows();

    /**
     * displayFooter() - displays the footer row information
     */
    void displayFooter();

    /**
     * displayRow() - displays a single row on the screen.
     * @param row - row number to be displayed.
     */
    void displayRow(int row);

    /**
     * writeDisplay() - Push data from the library to the ram of the Chip. Should
     * be called at a regular interval for smooth operation.
     */
    void writeDisplay();

    /**
     * setHeader() - Sets the header information for the current scene. Must be
     * 20 characters or less.
     * @param head - string to be inserted into the header.
     */
    void setHeader(const char head[]);

    /**
     * setRow() - sets the data of a particular row.
     * @param row - row to be written
     * @param text - string of no more than 20 characters to be written
     */
    void setRow(int row, const char text[]);

    /**
     * setTotalRows() - simple setter to set the total number of rows
     * @param n - number of rows
     */
    void setTotalRows(int n);

    /**
     * setFooterButton - access to the individual buttons that may be present
     * in the footer
     * @param btn which button
     * @param text the text of the button
     */
    void setFooterButton(int btn, const char text[]);

    /**
     * drawBox() - draws a single inverted box one character wide on the screen
     * @param col
     * @param row
     * @param w
     */
    void drawBox(uint8_t col, uint8_t row, uint8_t w);

    /**
     * scrollDown() - scrolls the screen down. Wrapper around scrollScreen()
     * @return true if able to scroll down, false otherwise
     */
    boolean scrollDown();

    /**
     * scrollUp() - scrolls the screen up. Wrapper around scrollScreen()
     * @return true if able to scroll up, false otherwise.
     */
    boolean scrollUp();

    /**
     * pressFooterButton() - fills the button with inverted box if pressed
     * @param btn - button to be pressed.
     */
    void pressFooterButton(int btn);

    /**
     * clearData() - clears all data held in the buffers and creates a blank screen
     */
    void clearData();

    /*
     * simple getter to return title
     */
    char *getTitle();
};

#endif //FORAS_SCREEN_H
