//
// Created by dhuck on 1/13/21.
//

#ifndef FORAS_PAGE_H
#define FORAS_PAGE_H

#include "controls.h"
#include "screen.h"
#include "preset.h"
#include <string>

/**
 * Page is an interface for any class that will change the layout of the screen.
 * All layouts for the main screen will need access to the screen, commandQueue,
 * and preset variables.
 */
class Page {
public:
    /**
     * checkMessages() - checks the commandQueue and changes behavior dependant
     * on the incoming command. Should be implementable as part of the base
     * class rather than each individual layout
     */
    virtual void checkMessages() = 0;

    /**
     * activate() - activates the current layout. Must be implemented by each
     * layout. Handles redrawing the screen for the currently layout.
     */
    virtual void activate() = 0;

    /**
     * deactivate() - handle any kind of extra details that need to be done
     * before the current layout gives up the screen to the next one.
     */
    virtual void deactivate() = 0;
};

#endif //FORAS_PAGE_H
