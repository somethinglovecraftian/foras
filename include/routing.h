//
// Created by dhuck on 1/13/21.
//

#ifndef FORAS_ROUTING_H
#define FORAS_ROUTING_H

#include "page.h"
#include "controls.h"
#include "screen.h"
#include "preset.h"

#define ROUTING_FILTER 3
#define ROUTING_ACTIVE_OUT 17

#define FILTER_WIDTH 4

class Routing : public Page {
private:
    Screen *screen;
    ControlQueue *commandQueue;
    Preset *preset;

    static const uint8_t ROWS = 4;
    char rows[ROWS][ROW_WIDTH];

    uint8_t activeRow = 0;

    uint8_t activeFilter = 0;
    uint8_t activeOutput = 0;

    char filterText[NUM_FILTERS][FILTER_WIDTH] = {
            {'C', 'C', ' ', ' '},
            {'N', 'O', 'T', ' '},
            {'C', 'L', 'K', ' '}
    };

    boolean active;

    void setRows();
    void setFilterInfo(uint8_t row, const bool* filters);
    void setOutputInfo(uint8_t row, const bool* outputs);
    void filterSelection(bool up);
    void outputSelection(bool up);
    void toggleSelection(bool filter);

public:
    explicit Routing(ControlQueue *, Screen *, Preset *);

    void checkMessages() override;

    void activate() override;
    void deactivate() override;

};


#endif //FORAS_ROUTING_H
