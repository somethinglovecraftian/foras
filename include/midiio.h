//
// Created by dhuck on 1/13/21.
//

#ifndef FORAS_MIDIIO_H
#define FORAS_MIDIIO_H

//#include "page.h"
#include "Arduino.h"
#include "preset.h"
#include "MIDI.h"



class MidiIo {
private:
    Preset *preset;
    midi::MidiInterface<midi::SerialMIDI<HardwareSerial>>* midi;
    midi::MidiInterface<midi::SerialMIDI<HardwareSerial>>* io[NUM_INPUTS];

public:
    MidiIo(Preset *);

    void flush();

    void parseMidi(uint8_t);
    bool applyFilters(byte type, uint8_t ch, uint8_t in);
};

#endif //FORAS_MIDIIO_H
