//
// Created by dhuck on 1/13/21.
//

#ifndef FORAS_PRESET_H
#define FORAS_PRESET_H

#include "Arduino.h"
#include "settings.h"
#include "EEPROM.h"

#define NUM_OUTPUTS 4
#define NUM_INPUTS 4
#define NUM_FILTERS 3
#define NUM_CHANNELS 16
#define NAME_LENGTH 8
#define PRESET_BUFFER 64
#define PRESET_START_ADDRESS 0x80
#define CC_FILTER 0
#define NOT_FILTER 1
#define CLK_FILTER 2
#define SETTING_ON 1
#define SETTING_OFF 0

enum ClockSource {
    INTERNAL, EXTERNAL_0, EXTERNAL_1, EXTERNAL_2, EXTERNAL_3
};
struct Channel {
    bool cc;
    bool note;
    bool sysex;
};

struct Output {
    bool input[NUM_INPUTS];
    bool enabled;
    Channel channels[NUM_CHANNELS];
};

class Preset {
private:
    Output settings[NUM_OUTPUTS];
    char name[NAME_LENGTH];
    ClockSource clockSource;
    uint8_t tempo; // MAX TEMPO IS 512 BPM

    byte packed_preset[PRESET_BUFFER];

    /**
     * pack() - packs the preset into a hyper compressed structure to be stored
     * in eeprom and flash memory.
     */
    void pack();

    /**
     * unpack() - reverses the packing from pack() when loading out of eeprom
     * or flash memory. Used in conjunction with
     */
    void unpack();
public:
    Preset();

    /**
     * load() - Loads preset from flash memory into packed_preset(). Must also
     * call unpack();
     */
    void load();

    /**
     * store() - stores packed_preset into a flash memory file location. Must first
     * call pack();
     */
    void store();

    /**
     * sync() - Syncs whatever is in packed_preset to eeprom memory. Only bytes
     * that differ from what is currently eeprom will be written to reduce
     * eeprom write wearing. To save computation, should only be called after
     * value is set.
     */
    void sync();

    /**
     * getSettings() - Gets a pointer to the settings of a specific output.
     * @return Output pointer
     */
    Output *getSettings(uint8_t);

    /**
     * updatesSettings() updates the setting of a particular output.
     * TODO: implement properly with new structure
     */
    void updateSetting(Settings, uint8_t, uint8_t);
    /**
     * toggleSettings - Turns bool based settings on or off.
     * TODO: proper implementation with new structure.
     */
    void toggleSetting(Settings, uint8_t);
};
#endif //FORAS_PRESET_H
